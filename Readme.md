
# Wrigo

Wrigo (as in "Writer for Hugo") is a simple tool to generate the metadata for your Hugo posts in the terminal.

## Features

1. Generates the following metadata:
    - Author
    - Title
    - Description
    - Current Date
    - Tags
2. Persists author and previous tag data in `~/.config/wrigo/config.yaml`. This way in the next prompt, you don't need to set author again and see last used tags.
3. Generates `<lower-case-of-your-title>.md` file in the current directory.
4. Makes starting a new post less of a hassle than using templates.

## Get started

```bash
# Clone this repo
git clone gitlab.com/systemscribe/wrigo
cd wrigo

# Build a binary in your $PATH
go build -o ~/.local/bin/wrigo cmd/main.go

# Generate a new markdown file with metadata
wrigo
```
