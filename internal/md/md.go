package md

import (
	"fmt"
	"os"
	"strings"
)

// Configuration struct to hold key-value pairs.
type Configuration struct {
	Author      string   `yaml:"author"`
	Title       string   `yaml:"title"`
	Date        string   `yaml:"date"`
	Description string   `yaml:"description"`
	Tags        []string `yaml:"tags"`
}

// Fill template with struct
func Generate(config Configuration) string {
	// Generate the Markdown content
	markdown := fmt.Sprintf(`+++
author = "%s"
title = "%s"
date = "%s"
description = "%s"
tags = %s
+++
`, config.Author, config.Title, config.Date, config.Description, config.Tags)

	return markdown
}

// Write metadata to file
func Write(markdown, name string) error {
	markdownFileString := strings.ReplaceAll(name, " ", "-")
	markdownFileString = strings.ToLower(markdownFileString)
	markdownFilePath := markdownFileString + ".md"
	err := os.WriteFile(markdownFilePath, []byte(markdown), 0644)
	if err != nil {
		return err
	}

	return nil
}
