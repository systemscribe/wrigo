package cfg

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gopkg.in/yaml.v3"

	"gitlab.com/systemscribe/wrigo/internal/md"
)

// Decrlare constants
const (
	configDirName  = "wrigo"
	configFileName = "config.yaml"
)

// Declare variables
var (
	ConfigDir  = Home()
	ConfigPath = filepath.Join(ConfigDir, configFileName)
)

// Initiate config directory from user home
func Home() string {
	// Get the current user's home directory
	usr, err := os.UserHomeDir()
	if err != nil {
		log.Fatalf("Error getting user's home directory: %v", err)
	}

	// Construct the path to the default config directory ~/.config/wrigo/config.yaml
	ConfigDir := filepath.Join(usr, ".config", configDirName)
	return ConfigDir
}

func Init() {
}

// Function to collect post metadata
func Get(configStatus bool) md.Configuration {
	var (
		config                                md.Configuration
		authorStr, titleStr, descStr, tagsStr string
	)

	reader := bufio.NewReader(os.Stdin)
	// If config file is not present
	// Ask for author and create config file
	if !configStatus {
		fmt.Print("Enter Author: ")
		authorStr, _ = reader.ReadString('\n')
		config.Author = strings.TrimSpace(authorStr)

		err := Write(ConfigPath, config)
		if err != nil {
			log.Fatalf("Error writing to file: %v", err)
		}
	}
	// Load metadata from file and update it with new values
	config, err := Read(ConfigPath)
	if err != nil {
		log.Fatalf("Error reading from file: %v", err)
	}

	fmt.Print("Enter Title: ")
	titleStr, _ = reader.ReadString('\n')
	config.Title = strings.TrimSpace(titleStr)

	fmt.Print("Enter Description: ")
	descStr, _ = reader.ReadString('\n')
	config.Description = strings.TrimSpace(descStr)

	// Set the date to the current date
	config.Date = time.Now().Format("2006-01-02")

	if configStatus {
		fmt.Print("Previous tags: ", config.Tags, "\n")
	}
	fmt.Print("Enter Tags (comma-separated): ")
	tagsStr, _ = reader.ReadString('\n')
	tagList := strings.Split(strings.TrimSpace(tagsStr), ",")
	config.Tags = make([]string, len(tagList))
	for i, tag := range tagList {
		config.Tags[i] = fmt.Sprintf(`"%s",`, tag)
	}

	return config
}

func Write(configPath string, config md.Configuration) error {
	data, err := yaml.Marshal(config)
	if err != nil {
		return err
	}

	err = os.WriteFile(configPath, data, 0644)
	if err != nil {
		return err
	}

	return nil
}

func Read(filePath string) (md.Configuration, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return md.Configuration{}, err
	}

	var config md.Configuration
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return md.Configuration{}, err
	}

	return config, nil
}
