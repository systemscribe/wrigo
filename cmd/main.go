package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/systemscribe/wrigo/internal/cfg"
	"gitlab.com/systemscribe/wrigo/internal/md"
)

// Main
func main() {
	// Check if the default config directory exists
	if _, err := os.Stat(cfg.ConfigPath); os.IsNotExist(err) {
		// Directory doesn't exist, create it
		if err := os.MkdirAll(cfg.ConfigDir, os.ModePerm); err != nil {
			log.Fatalf("Error creating config directory: %v", err)
		}
		// Ask the user for configuration values
		config := cfg.Get(false)

		// Write the configuration to the config file
		if err := cfg.Write(cfg.ConfigPath, config); err != nil {
			log.Fatalf("Error writing config file: %v", err)
		}
	} else {
		// Skip author prompt
		// Ask the user for the rest of the configuration values
		fmt.Println("Provide the following details:")
		config := cfg.Get(true)

		// Write the updated configuration to the config file
		if err := cfg.Write(cfg.ConfigPath, config); err != nil {
			log.Fatalf("Error writing config file: %v", err)
		}
	}

	// Generate Markdown and write to file
	config, err := cfg.Read(cfg.ConfigPath)
	if err != nil {
		log.Fatalf("Error reading config file: %v", err)
	}

	markdown := md.Generate(config)
	if err := md.Write(markdown, config.Title); err != nil {
		log.Fatalf("Error writing Markdown file: %v", err)
	}

	fmt.Println("Markdown file generated successfully.")
}
